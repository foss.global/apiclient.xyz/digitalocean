# @mojoio/digitalocean
an unofficial DigitalOcean API abstraction package by Lossless GmbH

## Availabililty and Links
* [npmjs.org (npm package)](https://www.npmjs.com/package/@mojoio/digitalocean)
* [gitlab.com (source)](https://gitlab.com/mojoio/digitalocean)
* [github.com (source mirror)](https://github.com/mojoio/digitalocean)
* [docs (typedoc)](https://mojoio.gitlab.io/digitalocean/)

## Status for master
[![build status](https://gitlab.com/mojoio/digitalocean/badges/master/build.svg)](https://gitlab.com/mojoio/digitalocean/commits/master)
[![coverage report](https://gitlab.com/mojoio/digitalocean/badges/master/coverage.svg)](https://gitlab.com/mojoio/digitalocean/commits/master)
[![npm downloads per month](https://img.shields.io/npm/dm/@mojoio/digitalocean.svg)](https://www.npmjs.com/package/@mojoio/digitalocean)
[![Known Vulnerabilities](https://snyk.io/test/npm/@mojoio/digitalocean/badge.svg)](https://snyk.io/test/npm/@mojoio/digitalocean)
[![TypeScript](https://img.shields.io/badge/TypeScript->=%203.x-blue.svg)](https://nodejs.org/dist/latest-v10.x/docs/api/)
[![node](https://img.shields.io/badge/node->=%2010.x.x-blue.svg)](https://nodejs.org/dist/latest-v10.x/docs/api/)
[![JavaScript Style Guide](https://img.shields.io/badge/code%20style-prettier-ff69b4.svg)](https://prettier.io/)

## Usage

For further information read the linked docs at the top of this readme.

> MIT licensed | **&copy;** [Lossless GmbH](https://lossless.gmbh)
| By using this npm module you agree to our [privacy policy](https://lossless.gmbH/privacy)

[![repo-footer](https://lossless.gitlab.io/publicrelations/repofooter.svg)](https://maintainedby.lossless.com)
